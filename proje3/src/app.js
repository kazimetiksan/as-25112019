const ListItem = (props) => {

    const {firstName, lastName, age=39} = props.person
    const {index,removeMethod,editMethod} = props

    return (
        <li>{firstName} {lastName}
            <button onClick={() => {
                editMethod(index)
            }}>Güncelle</button>
        <button onClick={() => {
                removeMethod(index)
            }}>Sil</button>
        </li>
    )
}

class ListingApp extends React.Component {

    constructor () {
        super()

        this.onSubmit = this.onSubmit.bind(this)
        this.onDelete = this.onDelete.bind(this)
        this.onEdit = this.onEdit.bind(this)
        this.onChangeFirstName = this.onChangeFirstName.bind(this)
        this.onChangeLastName = this.onChangeLastName.bind(this)

        this.state = {
            people: [{
                firstName: "Hasan",
                lastName: "Demir"
            },
            {
                firstName: "Meltem",
                lastName: "Tekin"
            },
            {
                firstName: "Rüya",
                lastName: "Bağcı"
            }],
            updatingIndex: undefined,
            firstName: '',
            lastName: ''
        }
    }

    onSubmit (e) {

        e.preventDefault()

        this.setState((prevState) => {

            const newPerson = {
                firstName: prevState.firstName,
                lastName: prevState.lastName
            }

            if (prevState.updatingIndex != undefined) {
                let people = prevState.people
                people.splice(prevState.updatingIndex, 1, newPerson)

                return {
                    people,
                    updatingIndex: undefined
                }
            } else {
                let people = prevState.people.concat([newPerson])

                return {
                    people
                }
            }
        })
    }

    onEdit (index) {

        console.log(`editing item at index: ${index}`)
        this.setState(() => {
            return {
                updatingIndex: index
            }
        }, () => {

            const updatingPerson = this.state.people[this.state.updatingIndex]
            console.log(updatingPerson)

            this.setState(() => {
                return {
                    firstName: updatingPerson.firstName,
                    lastName: updatingPerson.lastName
                }
            })            
        })
    }

    onDelete (index) {

        this.setState((prevState) => {

            const people = prevState.people.filter((person,i) => {
                return i != index
            })

            return {
                people
            }
        })
    }

    onChangeFirstName (e) {

        const val = e.target.value
        this.setState(() => {
            return {
                firstName: val
            }
        })
    }

    onChangeLastName (e) {

        const val = e.target.value
        this.setState(() => {
            return {
                lastName: val
            }
        })
    }

    render () {

        const {people} = this.state

        return (
            <div>
                <h1>Kullanıcılar</h1>
                <div>
                {
                    people.map((person,index) => {
                        return (
                            <ListItem 
                                key={index} 
                                person={person} 
                                index={index}
                                removeMethod={this.onDelete}
                                editMethod={this.onEdit}
                            />
                        )
                    })
                }
                </div>
                <div>
                    <form onSubmit={this.onSubmit}>
                        <input onChange={this.onChangeFirstName} value={this.state.firstName} placeholder="Ad" />
                        <input onChange={this.onChangeLastName} value={this.state.lastName} placeholder="Soyad" />
                        <button>
                        {
                            this.state.updatingIndex != undefined ? 'Güncelle' : 'Ekle'
                        }
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}

var root = document.getElementById('app')
ReactDOM.render(<ListingApp />, root)