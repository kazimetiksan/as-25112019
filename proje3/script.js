"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ListItem = function ListItem(props) {
    var _props$person = props.person,
        firstName = _props$person.firstName,
        lastName = _props$person.lastName,
        _props$person$age = _props$person.age,
        age = _props$person$age === undefined ? 39 : _props$person$age;
    var index = props.index,
        removeMethod = props.removeMethod,
        editMethod = props.editMethod;


    return React.createElement(
        "li",
        null,
        firstName,
        " ",
        lastName,
        React.createElement(
            "button",
            { onClick: function onClick() {
                    editMethod(index);
                } },
            "G\xFCncelle"
        ),
        React.createElement(
            "button",
            { onClick: function onClick() {
                    removeMethod(index);
                } },
            "Sil"
        )
    );
};

var ListingApp = function (_React$Component) {
    _inherits(ListingApp, _React$Component);

    function ListingApp() {
        _classCallCheck(this, ListingApp);

        var _this = _possibleConstructorReturn(this, (ListingApp.__proto__ || Object.getPrototypeOf(ListingApp)).call(this));

        _this.onSubmit = _this.onSubmit.bind(_this);
        _this.onDelete = _this.onDelete.bind(_this);
        _this.onEdit = _this.onEdit.bind(_this);
        _this.onChangeFirstName = _this.onChangeFirstName.bind(_this);
        _this.onChangeLastName = _this.onChangeLastName.bind(_this);

        _this.state = {
            people: [{
                firstName: "Hasan",
                lastName: "Demir"
            }, {
                firstName: "Meltem",
                lastName: "Tekin"
            }, {
                firstName: "Rüya",
                lastName: "Bağcı"
            }],
            updatingIndex: undefined,
            firstName: '',
            lastName: ''
        };
        return _this;
    }

    _createClass(ListingApp, [{
        key: "onSubmit",
        value: function onSubmit(e) {

            e.preventDefault();

            this.setState(function (prevState) {

                var newPerson = {
                    firstName: prevState.firstName,
                    lastName: prevState.lastName
                };

                if (prevState.updatingIndex != undefined) {
                    var people = prevState.people;
                    people.splice(prevState.updatingIndex, 1, newPerson);

                    return {
                        people: people,
                        updatingIndex: undefined
                    };
                } else {
                    var _people = prevState.people.concat([newPerson]);

                    return {
                        people: _people
                    };
                }
            });
        }
    }, {
        key: "onEdit",
        value: function onEdit(index) {
            var _this2 = this;

            console.log("editing item at index: " + index);
            this.setState(function () {
                return {
                    updatingIndex: index
                };
            }, function () {

                var updatingPerson = _this2.state.people[_this2.state.updatingIndex];
                console.log(updatingPerson);

                _this2.setState(function () {
                    return {
                        firstName: updatingPerson.firstName,
                        lastName: updatingPerson.lastName
                    };
                });
            });
        }
    }, {
        key: "onDelete",
        value: function onDelete(index) {

            this.setState(function (prevState) {

                var people = prevState.people.filter(function (person, i) {
                    return i != index;
                });

                return {
                    people: people
                };
            });
        }
    }, {
        key: "onChangeFirstName",
        value: function onChangeFirstName(e) {

            var val = e.target.value;
            this.setState(function () {
                return {
                    firstName: val
                };
            });
        }
    }, {
        key: "onChangeLastName",
        value: function onChangeLastName(e) {

            var val = e.target.value;
            this.setState(function () {
                return {
                    lastName: val
                };
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _this3 = this;

            var people = this.state.people;


            return React.createElement(
                "div",
                null,
                React.createElement(
                    "h1",
                    null,
                    "Kullan\u0131c\u0131lar"
                ),
                React.createElement(
                    "div",
                    null,
                    people.map(function (person, index) {
                        return React.createElement(ListItem, {
                            key: index,
                            person: person,
                            index: index,
                            removeMethod: _this3.onDelete,
                            editMethod: _this3.onEdit
                        });
                    })
                ),
                React.createElement(
                    "div",
                    null,
                    React.createElement(
                        "form",
                        { onSubmit: this.onSubmit },
                        React.createElement("input", { onChange: this.onChangeFirstName, value: this.state.firstName, placeholder: "Ad" }),
                        React.createElement("input", { onChange: this.onChangeLastName, value: this.state.lastName, placeholder: "Soyad" }),
                        React.createElement(
                            "button",
                            null,
                            this.state.updatingIndex != undefined ? 'Güncelle' : 'Ekle'
                        )
                    )
                )
            );
        }
    }]);

    return ListingApp;
}(React.Component);

var root = document.getElementById('app');
ReactDOM.render(React.createElement(ListingApp, null), root);
