import {createStore,applyMiddleware} from 'redux'
import {studentReducer} from '../reducers/reducers'
import thunk from 'redux-thunk'

export default () => {
    return createStore(
        studentReducer,
        applyMiddleware(thunk)
        )
}