import React from 'react'
import { NavLink } from "react-router-dom"

const Header = () => {
    return (
        <div>
            <NavLink to="/">Dashboard</NavLink>
            <NavLink to="/add">Yeni Ekle</NavLink>
            <NavLink to="/login">Giriş Yap</NavLink>
            <NavLink to="/signup">Kullanıcı Oluştur</NavLink>
        </div>
    )
}

export default Header