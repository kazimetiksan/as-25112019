import React, {useState} from 'react'

const Form = (props) => {

    const {onSubmit,editingStudent} = props

    const [firstName,setFirstName] = useState(editingStudent == undefined ? '' : editingStudent.firstName)
    const [lastName,setLastName] = useState(editingStudent == undefined ? '' : editingStudent.lastName)
    const [classroom,setClassroom] = useState(editingStudent == undefined ? '' : editingStudent.classroom)

    return (
        <form onSubmit={(e) => {

            e.preventDefault()

            onSubmit({
                firstName,
                lastName,
                classroom
            }, editingStudent)
        }}>
            <input name="firstName" 
                    placeholder="Ad"
                    value={firstName}
                    onChange={(e) => {
                        setFirstName(e.target.value)
                    }}
                />
            <input name="lastName" 
                    placeholder="Soyad"
                    value={lastName}
                    onChange={(e) => {
                        setLastName(e.target.value)
                    }}
                />
            <select name="classroom" value={classroom} onChange={(e) => {
                setClassroom(e.target.value)
            }}>
                <option value="arılar">Arılar</option>
                <option value="kelebekler">Kelebekler</option>
            </select>
            <div>
                <button>Kaydet</button>
            </div>            
        </form>
    )
}

export default Form