import React from 'react'
import validator from 'validator'
import axios from 'axios'

const Signup = (props) => {

    // const {onSubmit} = props

    const onSubmit = (e) => {

        e.preventDefault()

        if (!validator.isEmail(e.target.elements.email.value)) {
            alert('Lütfen email adresi girin')
            return 
        }

        const newUser = {
            name: e.target.elements.name.value,
            email: e.target.elements.email.value,
            password: e.target.elements.password.value
        }

        axios.post('https://std02.herokuapp.com/api/users',newUser)
        .then((response) => {
            console.log(response)
        })
        .catch((err) => {
            console.log(err)
        })
    }

    return (
        <div>
            <h1>Yeni Kullanıcı Oluştur</h1>
            <form onSubmit={onSubmit}>
                <input name="name" placeholder="Ad Soyad" /><br />
                <input name="email" placeholder="Email" /><br />
                <input name="password" placeholder="Password" /><br />
                <button>Kaydet</button>
            </form>
        </div>
    )
}

export default Signup