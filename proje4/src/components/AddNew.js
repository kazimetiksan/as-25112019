import React, {Fragment} from 'react'
import Form from './Form'
import {connect} from 'react-redux'
import {asyncAddAction, asyncEditAction} from '../actions/actions'
import {useParams} from 'react-router-dom'

class AddNew extends React.Component {

    componentDidMount () {
        console.log('add new did mount', this.props)
    }

    constructor (props) {
        super(props)

        this.onSubmit = this.onSubmit.bind(this)

        const {_id} = props.match.params
        
        this.state = {
            editingId: _id
        }
    }

    onSubmit (newStudent,editingStudent=undefined) {

        const {addNewStudent,editStudent,history} = this.props
        
        if (editingStudent == undefined) {
            addNewStudent(newStudent)
        } else {
            editStudent(newStudent,editingStudent._id)
        }
        history.push('/')
    }

    render () {

        const {students} = this.props
        const editingStudent = students.find((std) => {
            return std._id == this.state.editingId
        })

        return (

            <Fragment>
                <h1>Yeni Öğrenci Ekle</h1>
                <Form 
                    onSubmit={this.onSubmit} 
                    editingStudent={editingStudent}
                />
            </Fragment>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addNewStudent: (obj) => {dispatch(asyncAddAction(obj))},
        editStudent: (obj,editingId) => {dispatch(asyncEditAction(obj,editingId))}
    }
}

const mapStateToProps = (state) => {
    return {
        students: state
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AddNew)