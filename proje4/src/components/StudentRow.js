import React from 'react'

export default (props) => {

    const {student,onRemove,onEdit} = props

    return (
        <li>{student.firstName} {student.lastName} {student.classroom}
            <button onClick={(e) => {
                onEdit(student._id)
            }}>GÜNCELLE</button>
            <button onClick={(e) => {
                onRemove(student._id)
            }}>SİL</button>
        </li>
    )
}