import React from 'react'
import validator from 'validator'
import axios from 'axios'

const Login = (props) => {

    const onSubmit = (e) => {

        e.preventDefault()

        if (!validator.isEmail(e.target.elements.email.value)) {
            alert('Lütfen email adresi girin')
            return 
        }

        const loggedInUser = {
            email: e.target.elements.email.value,
            password: e.target.elements.password.value
        }

        axios.post('https://std02.herokuapp.com/api/users/login',loggedInUser)
        .then((response) => {
            console.log(response)
            const xauth = response.headers.xauth
            sessionStorage.setItem('xauth',xauth)

            // GETTER
            // sessionStorage.getItem('xauth')
        })
        .catch((err) => {
            console.log(err)
        })
    }

    return (
        <div>
            <h1>Kullanıcı Girişi</h1>
            <form onSubmit={onSubmit}>
                <input name="email" placeholder="Email" /><br />
                <input name="password" placeholder="Password" /><br />
                <button>Giriş Yap</button>
            </form>
        </div>
    )
}

export default Login