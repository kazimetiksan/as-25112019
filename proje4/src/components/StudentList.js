import React, {Fragment} from 'react'
import StudentRow from './StudentRow'
import {connect} from 'react-redux'
import {asyncRemoveAction,setAllAction} from '../actions/actions'
import axios from 'axios'

// useEffect

class StudentList extends React.Component {

    constructor (props) {
        super()

        this.onEdit = this.onEdit.bind(this)
        this.onRemove = this.onRemove.bind(this)
    }

    componentDidMount () {

        axios.get('https://std02.herokuapp.com/api/student')
        .then((response) => {
            
            this.props.loadAll(response.data.list)
        })
        .catch((err) => {
            console.log(err)
        })
    }

    onRemove (_id) {
        console.log('silinecek id: ',_id)
        removeStudent(_id)
    }

    onEdit (_id) {
        console.log('editlenecek id: ',_id)
        this.props.history.push(`/edit/${_id}`)
    }

    render () {

        const {students,removeStudent,history} = this.props

        return (
            <div>
            {
                students && students.map((std,index) => {
                    return <StudentRow 
                                key={index}
                                student={std}
                                onRemove={this.onRemove}
                                onEdit={this.onEdit}
                            />
                })
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        students: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeStudent: (_id) => {dispatch(asyncRemoveAction(_id))},
        loadAll: (data) => {dispatch(setAllAction(data))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(StudentList)