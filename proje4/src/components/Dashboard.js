import React, {Fragment} from 'react'
import StudentList from './StudentList'

class Dashboard extends React.Component {

    componentDidMount () {
        console.log('dashboard did mount', this.props)
    }

    render () {

        const {history} = this.props

        return (
            <Fragment>
                <h1>Öğrenci Listesi</h1>
                <StudentList history={history} />
            </Fragment>
        )
    }
}

export default Dashboard

// HIGHER ORDER COMPONENT

// export {class1, class2, class3, Dashboard as default}
