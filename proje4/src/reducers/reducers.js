import uuid from 'uuid'
import {ADD, EDIT, REMOVE, SET_ALL} from './identifiers'

// const demoState = [{
//     _id: uuid(),
//     firstName: "Hasan",
//     lastName: "Demir",
//     classroom: "arılar"
// },{
//     _id: uuid(),
//     firstName: "Elif",
//     lastName: "Tekin",
//     classroom: "arılar"
// },{
//     _id: uuid(),
//     firstName: "Kazım",
//     lastName: "Etiksan",
//     classroom: "arılar"
// }]

export const studentReducer = (state=[],action) => {

    switch (action.type) {
        case SET_ALL:
            const {data:students} = action
            return students

        case ADD:
            const {data:newStudent} = action
            return [...state, newStudent]

        case EDIT:
            return state.map((obj) => {
                if (obj._id == action.editingId) {
                    return {
                        ...obj,
                        ...action.data
                    }
                }
                return obj
            })

        case REMOVE:
            return state.filter((obj) => {
                return obj._id != action.data
            })

        default: return state
    }
}