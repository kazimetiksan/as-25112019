import uuid from 'uuid'
import {ADD,REMOVE,EDIT,SET_ALL} from '../reducers/identifiers'
import axios from 'axios'

export const setAllAction = (data = []) => {
    return {
        type: SET_ALL,
        data
    }
}

export const asyncAddAction = ({
    firstName='Default First',
    lastName='Default Last',
    classroom='arılar'
} = {}) => {
    return (dispatch) => {

        axios.post('https://std02.herokuapp.com/api/student',{
           firstName,
           lastName,
           classroom
       })
       .then((response) => {
           
            dispatch(addAction(response.data.data[0]))
        })
       .catch((err) => {
           console.log(err)
       })
    }
}

export const addAction = ({
    firstName='Default First',
    lastName='Default Last',
    classroom='arılar'
} = {}) => {
    return {
        type: ADD,
        data: {
            _id: uuid(),
            firstName,
            lastName,
            classroom
        }
    }
}

export const editAction = (data = {}, editingId) => {
    return {
        type: EDIT,
        editingId,
        data
    }
}

export const asyncEditAction = (data = {}, editingId) => {
    return (dispatch) => {

        axios.patch(
            `https://std02.herokuapp.com/api/student/${editingId}`,
            data
        )
       .then((response) => {
           
            dispatch(editAction(response.data.data[0], editingId)) 
        })
       .catch((err) => {
           console.log(err)
       })
    }
}

export const removeAction = (_id) => {
    return {
        type: REMOVE,
        data: _id
    }
}

export const asyncRemoveAction = (_id) => {
    return (dispatch) => {

        axios.delete(`https://std02.herokuapp.com/api/student/${_id}`)
       .then((response) => {
           
            dispatch(removeAction(_id))
        })
       .catch((err) => {
           console.log(err)
       })
    }
}