console.log('redux js loaded')

import {createStore} from 'redux'
import uuid from 'uuid'

const demoState = [{
    _id: uuid(),
    firstName: "Hasan",
    lastName: "Demir",
    classroom: "arılar"
}]

const store = createStore((state=demoState,action) => {

    switch (action.type) {
        case 'ADD':
            return [...state, action.data]

        case 'EDIT':
            return state.map((obj) => {
                if (obj._id == action.editingId) {
                    return {
                        ...obj,
                        ...action.data
                    }
                }
                return obj
            })

        case 'REMOVE':
            return state.filter((obj) => {
                return obj._id != action.data
            })

        default: return state
    }
})

store.subscribe(() => {
    console.log(store.getState())
})

const addAction = ({
    firstName='Default First',
    lastName='Default Last',
    classroom='arılar'
} = {}) => {
    return {
        type: 'ADD',
        data: {
            _id: uuid(),
            firstName,
            lastName,
            classroom
        }
    }
}

store.dispatch(addAction())

store.dispatch({
    type: 'ADD',
    data: {
        _id: uuid(),
        firstName: "Mehmet",
        lastName: "Bağcı",
        classroom: "kelebekler"
    }
})

store.dispatch({
    type: 'ADD',
    data: {
        _id: uuid(),
        firstName: "Hakan",
        lastName: "Demir",
        classroom: "kelebekler"
    }
})

const removingItem = store.getState()[1]

store.dispatch({
    type: 'REMOVE',
    data: removingItem._id
})

const editingItem = store.getState()[0]

store.dispatch({
    type: 'EDIT',
    data: {
        firstName: "Tekin"
    },
    editingId: editingItem._id
})
