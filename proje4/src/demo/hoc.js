// HIGHER ORDER COMPONENT

import React from 'react'
import ReactDOM from 'react-dom'

const SomeComponent = (props) => {
    return (
        <h3>Title: {props.text}</h3>
    )
}

const withWarning = (WrappedComponent) => {
    return (props) => (
        <div>
            <WrappedComponent {...props}/>
            <div>API connection failed</div>
        </div>
    )
}

const APIFailed = true
const LastComponent = APIFailed ? withWarning(SomeComponent) : SomeComponent

const root = document.getElementById('app')
ReactDOM.render(<LastComponent text="Hello World" />, root)


