import React from 'react'
import ReactDOM from 'react-dom'
import Dashboard from './components/Dashboard'
import AddNew from './components/AddNew'
import Header from './components/Header'
import {BrowserRouter,Route,Switch} from 'react-router-dom'
import configureStore from './store/store'
import {Provider} from 'react-redux'
import Signup from './components/Signup'
import Login from './components/Login'
import {Private} from './routes/routes'

const store = configureStore()

class RootApp extends React.Component {

    render () {

        return (
            <BrowserRouter>
                <Provider store={store}>
                    <Header />
                    <Switch>
                        <Route path="/" component={Dashboard} exact={true} />
                        <Private path="/add" component={AddNew} />
                        <Route path="/edit/:_id" component={AddNew} />
                        <Route path="/signup" component={Signup} />
                        <Route path="/login" component={Login} />
                        <Route path="/hello" render={() => {
                            return <div>Hello World</div>
                        }} />
                    </Switch>
                </Provider>
            </BrowserRouter>
        )
    }
}

var root = document.getElementById('app')
ReactDOM.render(<RootApp />, root)