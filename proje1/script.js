"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Counter = function (_React$Component) {
    _inherits(Counter, _React$Component);

    function Counter() {
        _classCallCheck(this, Counter);

        var _this = _possibleConstructorReturn(this, (Counter.__proto__ || Object.getPrototypeOf(Counter)).call(this));

        _this.state = {
            count: 0
        };

        _this.onIncrease = _this.onIncrease.bind(_this);
        _this.onDecrease = _this.onDecrease.bind(_this);
        _this.onReset = _this.onReset.bind(_this);
        return _this;
    }

    _createClass(Counter, [{
        key: "onReset",
        value: function onReset() {

            this.setState(function () {
                return {
                    count: 0
                };
            });
        }
    }, {
        key: "onDecrease",
        value: function onDecrease() {

            this.setState(function (prevState) {
                return {
                    count: prevState.count - 1
                };
            });
        }
    }, {
        key: "onIncrease",
        value: function onIncrease() {
            var _this2 = this;

            // this.count = this.count+1

            // Arrow Function
            // (param1,param2) => {return;}

            // this.setState({count: this.state.count+1})

            // this.setState((prevState) => {
            //     return {
            //         city: "Ankara"
            //     }
            // }, () => {
            //     console.log(`button clicked: ${this.state.count}`)
            // })

            this.setState(function (prevState) {
                return {
                    count: prevState.count + 1
                };
            }, function () {
                console.log("button clicked: " + _this2.state.count);
            });
        }
    }, {
        key: "render",
        value: function render() {

            return React.createElement(
                "div",
                null,
                React.createElement(
                    "h1",
                    null,
                    this.props.title
                ),
                React.createElement(
                    "p",
                    null,
                    "Current Count: ",
                    this.state.count
                ),
                React.createElement(
                    "button",
                    { onClick: this.onDecrease },
                    "Azalt"
                ),
                React.createElement(
                    "button",
                    { onClick: this.onReset },
                    "S\u0131f\u0131rla"
                ),
                React.createElement(
                    "button",
                    { onClick: this.onIncrease },
                    "Artt\u0131r"
                )
            );
        }
    }]);

    return Counter;
}(React.Component);

var root = document.getElementById('app');
ReactDOM.render(React.createElement(Counter, { title: "Hello World" }), root);
