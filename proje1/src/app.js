class Counter extends React.Component {

    constructor () {
        super()

        this.state = {
            count: 0
        }

        this.onIncrease = this.onIncrease.bind(this)
        this.onDecrease = this.onDecrease.bind(this)
        this.onReset = this.onReset.bind(this)
    }

    onReset () {

        this.setState(() => {
            return {
                count: 0
            }
        })
    }

    onDecrease () {

        this.setState((prevState) => {
            return {
                count: prevState.count-1
            }
        })
    }

    onIncrease () {

        // this.count = this.count+1

        // Arrow Function
        // (param1,param2) => {return;}

        // this.setState({count: this.state.count+1})

        // this.setState((prevState) => {
        //     return {
        //         city: "Ankara"
        //     }
        // }, () => {
        //     console.log(`button clicked: ${this.state.count}`)
        // })

        this.setState((prevState) => {
            return {
                count: prevState.count+1
            }
        }, () => {
            console.log(`button clicked: ${this.state.count}`)
        })
    }

    render () {

        return (
            <div>
                <h1>{this.props.title}</h1>
                <p>Current Count: {this.state.count}</p>
                <button onClick={this.onDecrease}>Azalt</button>
                <button onClick={this.onReset}>Sıfırla</button>
                <button onClick={this.onIncrease}>Arttır</button>
            </div>
        )
    }
}

var root = document.getElementById('app')
ReactDOM.render(<Counter title="Hello World" />, root)