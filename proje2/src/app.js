// class ListingApp extends React.Component {

//     constructor () {
//         super()

//         this.onChangeFirstName = this.onChangeFirstName.bind(this)
//         this.onChangeLastName = this.onChangeLastName.bind(this)
//         this.onSubmit = this.onSubmit.bind(this)

//         this.state = {
//             people: [],
//             firstName: '',
//             lastName: ''
//         }
//     }

//     onSubmit (e) {

//         e.preventDefault()

//         this.setState((prevState) => {

//             const newPerson = {
//                 firstName: prevState.firstName,
//                 lastName: prevState.lastName
//             }

//             return {
//                 people: prevState.people.concat([newPerson])
//             }
//         })
//     }

//     onChangeFirstName (e) {
//         console.log(e.target.value)

//         const firstName = e.target.value
        
//         this.setState(() => {
//             return {
//                 firstName
//             }
//         })
//     }

//     onChangeLastName (e) {

//         const lastName = e.target.value
        
//         this.setState(() => {
//             return {
//                 lastName
//             }
//         })
//     }

//     render () {

//         return (
//             <div>
//                 <h1>Kullanıcılar</h1>
//                 <div>
//                 {
//                     this.state.people.map((person,index) => {
//                         return (
//                             <li key={index}>{person.firstName} {person.lastName}</li>
//                         )
//                     })
//                 }
//                 </div>
//                 <div>
//                     <form onSubmit={this.onSubmit}>
//                         <input value={this.state.firstName} onChange={this.onChangeFirstName} placeholder="Ad" />
//                         <input value={this.state.lastName} onChange={this.onChangeLastName} placeholder="Soyad" />
//                         <button>Ekle</button>
//                     </form>
//                 </div>
//             </div>
//         )
//     }
// }

        // const city = {
        //     name: "İstanbul",
        //     code: 34,
        //     region: "Marmara"
        // }

        // const person = {
        //     name: "Mehmet"
        // }
        
        // var {name,code} = city
        // var {name} = person

        // console.log(name)

// class ListItem extends React.Component {

//     render () {

//         const {firstName, lastName, age=39} = this.props.person
//         const {index,removeMethod} = this.props

//         return (
//             <li>{firstName} {lastName}
//                 <button onClick={() => {
//                     removeMethod(index)
//                 }}>Sil</button>
//             </li>
//         )
//     }
// }

const ListItem = (props) => {

    const {firstName, lastName, age=39} = props.person
    const {index,removeMethod} = props

    return (
        <li>{firstName} {lastName} {age}
            <button onClick={() => {
                removeMethod(index)
            }}>Sil</button>
        </li>
    )
}

class ListingApp extends React.Component {

    constructor () {
        super()

        this.onSubmit = this.onSubmit.bind(this)
        this.onDelete = this.onDelete.bind(this)

        this.state = {
            people: []
        }
    }

    onSubmit (e) {

        e.preventDefault()

        const firstName = e.target.elements.firstName.value
        const lastName = e.target.elements.lastName.value

        this.setState((prevState) => {

            const newPerson = {
                firstName,
                lastName
            }

            return {
                people: prevState.people.concat([newPerson])
            }
        })
    }

    onDelete (index) {

        this.setState((prevState) => {

            // let people = prevState.people
            // people.splice(index, 1)

            const people = prevState.people.filter((person,i) => {
                return i != index
            })

            return {
                people
            }
        })
    }

    render () {

        const {people} = this.state

        return (
            <div>
                <h1>Kullanıcılar</h1>
                <div>
                {
                    people.map((person,index) => {
                        return (
                            <ListItem 
                                key={index} 
                                person={person} 
                                index={index}
                                removeMethod={this.onDelete}
                            />
                        )
                    })
                }
                </div>
                <div>
                    <form onSubmit={this.onSubmit}>
                        <input name="firstName" placeholder="Ad" />
                        <input name="lastName" placeholder="Soyad" />
                        <button>Ekle</button>
                    </form>
                </div>
            </div>
        )
    }
}

var root = document.getElementById('app')
ReactDOM.render(<ListingApp />, root)