"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// class ListingApp extends React.Component {

//     constructor () {
//         super()

//         this.onChangeFirstName = this.onChangeFirstName.bind(this)
//         this.onChangeLastName = this.onChangeLastName.bind(this)
//         this.onSubmit = this.onSubmit.bind(this)

//         this.state = {
//             people: [],
//             firstName: '',
//             lastName: ''
//         }
//     }

//     onSubmit (e) {

//         e.preventDefault()

//         this.setState((prevState) => {

//             const newPerson = {
//                 firstName: prevState.firstName,
//                 lastName: prevState.lastName
//             }

//             return {
//                 people: prevState.people.concat([newPerson])
//             }
//         })
//     }

//     onChangeFirstName (e) {
//         console.log(e.target.value)

//         const firstName = e.target.value

//         this.setState(() => {
//             return {
//                 firstName
//             }
//         })
//     }

//     onChangeLastName (e) {

//         const lastName = e.target.value

//         this.setState(() => {
//             return {
//                 lastName
//             }
//         })
//     }

//     render () {

//         return (
//             <div>
//                 <h1>Kullanıcılar</h1>
//                 <div>
//                 {
//                     this.state.people.map((person,index) => {
//                         return (
//                             <li key={index}>{person.firstName} {person.lastName}</li>
//                         )
//                     })
//                 }
//                 </div>
//                 <div>
//                     <form onSubmit={this.onSubmit}>
//                         <input value={this.state.firstName} onChange={this.onChangeFirstName} placeholder="Ad" />
//                         <input value={this.state.lastName} onChange={this.onChangeLastName} placeholder="Soyad" />
//                         <button>Ekle</button>
//                     </form>
//                 </div>
//             </div>
//         )
//     }
// }

// const city = {
//     name: "İstanbul",
//     code: 34,
//     region: "Marmara"
// }

// const person = {
//     name: "Mehmet"
// }

// var {name,code} = city
// var {name} = person

// console.log(name)

// class ListItem extends React.Component {

//     render () {

//         const {firstName, lastName, age=39} = this.props.person
//         const {index,removeMethod} = this.props

//         return (
//             <li>{firstName} {lastName}
//                 <button onClick={() => {
//                     removeMethod(index)
//                 }}>Sil</button>
//             </li>
//         )
//     }
// }

var ListItem = function ListItem(props) {
    var _props$person = props.person,
        firstName = _props$person.firstName,
        lastName = _props$person.lastName,
        _props$person$age = _props$person.age,
        age = _props$person$age === undefined ? 39 : _props$person$age;
    var index = props.index,
        removeMethod = props.removeMethod;


    return React.createElement(
        "li",
        null,
        firstName,
        " ",
        lastName,
        " ",
        age,
        React.createElement(
            "button",
            { onClick: function onClick() {
                    removeMethod(index);
                } },
            "Sil"
        )
    );
};

var ListingApp = function (_React$Component) {
    _inherits(ListingApp, _React$Component);

    function ListingApp() {
        _classCallCheck(this, ListingApp);

        var _this = _possibleConstructorReturn(this, (ListingApp.__proto__ || Object.getPrototypeOf(ListingApp)).call(this));

        _this.onSubmit = _this.onSubmit.bind(_this);
        _this.onDelete = _this.onDelete.bind(_this);

        _this.state = {
            people: []
        };
        return _this;
    }

    _createClass(ListingApp, [{
        key: "onSubmit",
        value: function onSubmit(e) {

            e.preventDefault();

            var firstName = e.target.elements.firstName.value;
            var lastName = e.target.elements.lastName.value;

            this.setState(function (prevState) {

                var newPerson = {
                    firstName: firstName,
                    lastName: lastName
                };

                return {
                    people: prevState.people.concat([newPerson])
                };
            });
        }
    }, {
        key: "onDelete",
        value: function onDelete(index) {

            this.setState(function (prevState) {

                // let people = prevState.people
                // people.splice(index, 1)

                var people = prevState.people.filter(function (person, i) {
                    return i != index;
                });

                return {
                    people: people
                };
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            var people = this.state.people;


            return React.createElement(
                "div",
                null,
                React.createElement(
                    "h1",
                    null,
                    "Kullan\u0131c\u0131lar"
                ),
                React.createElement(
                    "div",
                    null,
                    people.map(function (person, index) {
                        return React.createElement(ListItem, {
                            key: index,
                            person: person,
                            index: index,
                            removeMethod: _this2.onDelete
                        });
                    })
                ),
                React.createElement(
                    "div",
                    null,
                    React.createElement(
                        "form",
                        { onSubmit: this.onSubmit },
                        React.createElement("input", { name: "firstName", placeholder: "Ad" }),
                        React.createElement("input", { name: "lastName", placeholder: "Soyad" }),
                        React.createElement(
                            "button",
                            null,
                            "Ekle"
                        )
                    )
                )
            );
        }
    }]);

    return ListingApp;
}(React.Component);

var root = document.getElementById('app');
ReactDOM.render(React.createElement(ListingApp, null), root);
